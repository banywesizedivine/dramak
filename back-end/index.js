const express = require('express')
const app = express()
const cors = require('cors');
app.use(cors());
// string contenant le chemin jusqu'a la clé import de la cle compte de service
const serviceAccount = require('./dramak-f138d-firebase-adminsdk-g8z1o-3e7d66cf6f.json')
const fs = require('firebase-admin');
fs.initializeApp({
        credential: fs.credential.cert(serviceAccount)
    })
    // import firebase

const db = fs.firestore();

app.use(express.json())
const port = 3000
const joi = require('joi')
const movieSchema = joi.object({
    title: joi.string().min(2).required(),
    director: joi.string().min(3).required(),
    poster: joi.string().uri().required(),
    year: joi.number().min(1895).integer().required()
})
app.post('/movies', async(req, res) => {
        const moviesCollection = await db.collection('movies');
        const validation = movieSchema.validate(req.body);
        if (validation.hasOwnProperty("error")) {
            res.send(validation.error);
        } else {
            let result = await moviesCollection.add(req.body);
            res.send(result.id);
        }
    })
    // requete a la racine async avanyt de recuperer tous les elemts de la variable
app.get('/movies', async(req, res) => {
    // recuperer la collection movies
    const moviesCollection = await db.collection('movies').get();
    // on traite  chaque document de la collection. on va applique.. chaque elemnt d ela collextion va appeler une fonction date ... doc => on dezzipe le doc
    let movies = moviesCollection.docs.map(doc => doc.data());
    //envoyer les infos sur la collection à movies
    res.send(movies)

})

//listen toujours à la fin
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})